<?php

session_start();

class TaskList{
	//method for adding a new task
	public function add($description){
		//create a variable $newTask
		$newTask = (object)[
			"description" => $description,
			"isFinished" => false
		];

		//check if a session variable named tasks
		//if not, create a new one that is an empty array
		if($_SESSION["tasks"] === null){
			$_SESSION["tasks"] = array();
		}

		//push the $newTask object to aour session array
		array_push($_SESSION["tasks"], $newTask);
	}

	//method for updating
	public function update($id, $description, $isFinished){
		$_SESSION["tasks"][$id]->description = $description;
		$_SESSION["tasks"][$id]->isFinished = ($isFinished !== null) ? true : false;
	}
	public function remove($id){
		array_splice($_SESSION["tasks"], $id, 1);
	}
}

$taskList = new TaskList();


if($_POST["action"] === "add") {
	$taskList->add($_POST["description"]);
}else if($_POST["action"] === "update") {
	$taskList->update($_POST["id"], $_POST["description"], $_POST["isFinished"]);
}else if($_POST["action"] === "remove") {
	$taskList->remove($_POST["id"]);
}

//redirect that user back to index
header("Location: ./index.php");